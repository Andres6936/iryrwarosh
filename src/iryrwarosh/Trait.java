package iryrwarosh;

public enum Trait
{
    WALKER("walker"), SWIMMER("swimmer"), FLIER("flier"), JUMPER("jumper"),
    EXTRA_HP("healty"), STRONG_ATTACK("strong"), EXTRA_EVADE("evasive"), EXTRA_DEFENSE("shelled"),
    DOUBLE_ATTACK("double attack"), DOUBLE_MOVE("double move"),
    SPIKED("spiked"), REGENERATES("regenerates"), POISONOUS("poisonous"), CAMOUFLAGED("camouflaged"),
    AGGRESSIVE("aggressive"), HIDER("hider"), TERRITORIAL("territorial"), ROCK_SPITTER("rock spitter"),
    SOCIAL("social"), LOOTLESS("lootless"),
    EVADE_ATTACK("evasive attacker"), COUNTER_ATTACK("counter attack"),
    REACH_ATTACK("long reach"), DETECT_CAMOUFLAGED("perceptive"),
    DEFLECT_RANGED("deflects projectiles"), DEFLECT_MELEE("blocks melee"),
    HUNTER("hunter"), MYSTERIOUS("mysterious"), KNOCKBACK("knockback"), SLOWING_ATTACK("slowing attack");

    //---------------------------------
    // Atributos
    //---------------------------------

    /**
     * La descripcion del Trait.
     */
    private String description;

    //---------------------------------
    // Método Getter
    //---------------------------------

    /**
     * @return La descripción del Trait
     */
    public String getDescription()
    {
        return description;
    }

    //---------------------------------
    // Constructor
    //---------------------------------

    /**
     * Construye un Trait con la descripción pasada por parámetro. <br>
     * <i>Post: Se inicializa un Trait con la descripción correspondiente.</i>
     * @param description La descripción del Trait.
     */
    Trait(String description)
    {
        this.description = description;
    }

    //---------------------------------
    // Métodos de Clase
    //---------------------------------

    public static Trait getRandom()
    {
        return Trait.values()[(int) (Math.random() * Trait.values().length)];
    }
}
