package iryrwarosh.screens;

import java.awt.event.KeyEvent;

import asciiPanel.AsciiPanel;

public class DeadScreen implements Screen
{

    //---------------------------------
    // Atributos
    //---------------------------------

    private Screen previous;

    /**
     * El mensaje con la causa de muerte del jugador (Player).
     */
    private String causeOfDeath;

    //---------------------------------
    // Constructor
    //---------------------------------

    public DeadScreen(Screen previous, String causeOfDeath)
    {
        this.previous = previous;
        this.causeOfDeath = causeOfDeath;
    }

    //---------------------------------
    // Métodos
    //---------------------------------

    @Override
    public void displayOutput(AsciiPanel terminal)
    {
        previous.displayOutput(terminal);

        terminal.writeCenter("-- " + causeOfDeath + " --", 2, AsciiPanel.brightWhite);
        terminal.writeCenter("-- Presione [Enter] para reiniciar --", 3, AsciiPanel.brightWhite);
    }

    @Override
    public Screen respondToUserInput(KeyEvent key)
    {
        return key.getKeyCode() == KeyEvent.VK_ENTER ? new ChooseStartingItemsScreen() : this;
    }
}
