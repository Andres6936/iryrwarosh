package iryrwarosh;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Clase que representa un punto en el plano con coordenadas (x, y).
 */
public class Point
{
    //---------------------------------
    // Atributos
    //---------------------------------

    /**
     * Coordenada en el eje X.
     */
    public int x;

    /**
     * Coordenada en el eje Y.
     */
    public int y;

    //---------------------------------
    // Constructor
    //---------------------------------

    /**
     * Constructor de la clase.
     * Representa un punto en el plano con coordenadas (x, y).
     * @param x Coordenada en el eje x.
     * @param y Coordenada en el eje y.
     */
    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    //---------------------------------
    // Métodos
    //---------------------------------

    /**
     * Copia la coordenadas (x, y) del punto.
     * @return Un punto con coordenadas (x, y).
     */
    public Point copy()
    {
        return new Point(x, y);
    }

    /**
     * Devuelve la distancia total hasta el punto objetivo.
     * @param other El punto el cual se dea calcular la distancia
     * @return Devuelve un entero con la distancia total del objetivo.
     */
    public int distanceTo(Point other)
    {
        return Math.max(Math.abs(x - other.x), Math.abs(y - other.y));
    }

    /**
     * Devuelve una lista con ocho puntos que se encuentran adyacentes al punto actual.
     * @return Devuelve una lista con todos los puntos que se encuentran adyacentes al punto actual.
     */
    public List<Point> neighbors()
    {
        List<Point> neighbors = Arrays.asList(
                new Point(x - 1, y - 1), new Point(x + 0, y - 1), new Point(x + 1, y - 1),
                new Point(x - 1, y + 0), new Point(x + 1, y + 0),
                new Point(x - 1, y + 1), new Point(x + 0, y + 1), new Point(x + 1, y + 1));

        Collections.shuffle(neighbors);
        return neighbors;
    }

    /**
     * Suma los valores pasados por parámetro al las coordendas en (x, y) del punto actual.
     * @param dx El valor en x que se desea sumar a la coordenda en x punto.
     * @param dy El valor en y que se desa sumar a la coordenada en y punto.
     * @return El nuevo punto con los valores en (x, y) ya sumados.
     */
    public Point plus(int dx, int dy)
    {
        return new Point(x + dx, y + dy);
    }

    /**
     * Suma las coordendas en (x, y) de dos puntos.
     * @param other Punto que se desa sumar.
     * @return Un nuevo punto como resultado de la suma de dos puntos.
     */
    public Point plus(Point other)
    {
        return new Point(x + other.x, y + other.y);
    }

    /**
     * Resta las coordendas en (x, y) de dos puntos.
     * @param other Punto que se desa restar.
     * @return Un nuevo punto como resultado de la resta de dos puntos.
     */
    public Point minus(Point other)
    {
        return new Point(x - other.x, y - other.y);
    }
}
