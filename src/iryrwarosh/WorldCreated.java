package iryrwarosh;


public class WorldCreated extends Message
{
    /**
     * Instancia del jugador en la clase {WorldCreated}.
     */
    public Creature player;

    public WorldCreated(World world, Creature player, String text)
    {
        super(world, text);
        this.player = player;
    }

    @Override
    public boolean involves(Creature player)
    {
        return false;
    }

}
