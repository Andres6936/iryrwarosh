package iryrwarosh;

import iryrwarosh.screens.Screen;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Item
{

    //---------------------------------
    // Atributos
    //---------------------------------

    private int collectableValue;

    private Boolean canBePickedUp = true;

    /**
     * El nombre del Item.
     */
    private String name;

    /**
     * El simbolo del Item.
     */
    private char glyph;

    /**
     * El color del Item.
     */
    private Color color;

    /**
     * La descripción del Item.
     */
    private String description;

    private List<Trait> traits;

    private int evasionModifier;

    //---------------------------------
    // Métodos Getter
    //---------------------------------

    public int collectableValue()
    {
        return collectableValue;
    }

    public Boolean canBePickedUp()
    {
        return canBePickedUp;
    }

    /**
     * @return El nombre del Item.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return El simbolo del Item.
     */
    public char getGlyph()
    {
        return glyph;
    }

    /**
     * @return El color del Item.
     */
    public Color getColor()
    {
        return color;
    }

    /**
     * @return La descripción del Item.
     */
    public String getDescription()
    {
        return description;
    }

    public int evasionModifier()
    {
        return evasionModifier;
    }

    //---------------------------------
    // Métodos Setter
    //---------------------------------

    public void collectableValue(int value)
    {
        collectableValue = value;
    }

    public void setCanBePickedUp(Boolean value)
    {
        canBePickedUp = value;
    }

    //---------------------------------
    // Constructor
    //---------------------------------

    Item(String name, int glyph, Color color, String description)
    {
        this(name, glyph, color, 0, description);
    }

    Item(String name, int glyph, Color color, int evasionModifier, String description)
    {
        this.name = name;
        this.glyph = (char) glyph;
        this.color = color;
        this.traits = new ArrayList<Trait>();
        this.description = description;
        this.evasionModifier = evasionModifier;
    }

    //---------------------------------
    // Métodos
    //---------------------------------

    public void addTrait(Trait trait)
    {
        traits.add(trait);
    }

    public void removeTrait(Trait trait)
    {
        traits.remove(trait);
    }

    public boolean hasTrait(Trait trait)
    {
        return traits.contains(trait);
    }

    public void update(World world, Creature owner)
    {

    }

    public void onCollide(World world, Creature colider)
    {

    }

    public Screen use(Screen screen, World world, Creature player)
    {
        return screen;
    }
}
