package iryrwarosh;

public class WorldScreen
{
    //---------------------------------
    // Atributos
    //---------------------------------

    public static final int WALL = 0;
    public static final int TOP_LEFT = 1;
    public static final int CENTER = 2;
    public static final int BOTTOM_RIGHT = 3;
    public static final int WIDE = 4;

    public Tile defaultGround = Tile.GREEN_DIRT;
    public Tile defaultWall = Tile.GREEN_ROCK;
    public int nEdge = WALL;
    public int sEdge = WALL;
    public int wEdge = WALL;
    public int eEdge = WALL;

    public Boolean nWater = false;
    public Boolean sWater = false;
    public Boolean wWater = false;
    public Boolean eWater = false;
    public Boolean nwWater = false;
    public Boolean neWater = false;
    public Boolean swWater = false;
    public Boolean seWater = false;
    public Boolean canAddQuarterSection = true;
}
