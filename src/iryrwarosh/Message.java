package iryrwarosh;

public abstract class Message
{

    //---------------------------------
    // Atributos
    //---------------------------------

    /**
     * La instancia del mundo {World}.
     */
    public World world;

    /**
     * El texto del mensaje.
     */
    private String text;

    //---------------------------------
    // Métodos Getter
    //---------------------------------

    /**
     * @return El texto del mensaje.
     */
    public String getText()
    {
        return text;
    }

    //---------------------------------
    // Constructor
    //---------------------------------

    public Message(World world, String text)
    {
        this.world = world;
        this.text = text;
    }

    //---------------------------------
    // Métodos
    //---------------------------------

    abstract public boolean involves(Creature player);

    protected static String addArticle(String article, String word)
    {
        if (Character.isUpperCase(word.charAt(0)))
            return word;
        else
            return article + " " + word;
    }
}
