package iryrwarosh;

import iryrwarosh.screens.StartScreen;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

import asciiPanel.AsciiPanel;

public class ApplicationMain extends JFrame implements KeyListener
{
    //---------------------------------
    // Atributos
    //---------------------------------

    private static final long serialVersionUID = 1L;

    private AsciiPanel terminal;

    private iryrwarosh.screens.Screen screen;

    //---------------------------------
    // Constructor
    //---------------------------------

    public ApplicationMain()
    {
        super();
        terminal = new AsciiPanel();
        terminal.setDefaultBackgroundColor(Common.guiBackground);
        terminal.setDefaultForegroundColor(Common.guiForeground);
        add(terminal);
        pack();
        screen = new StartScreen();
        addKeyListener(this);
        repaint();
    }

    //---------------------------------
    // Métodos
    //---------------------------------

    @Override
    public void repaint()
    {
        terminal.clear();
        screen.displayOutput(terminal);
        super.repaint();
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        screen = screen.respondToUserInput(e);
        repaint();
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
    }

    @Override
    public void keyTyped(KeyEvent e)
    {
    }

    //---------------------------------
    // Método Main
    //---------------------------------

    /**
     * Método principal, se encarga de ejecutar la App.
     * @param args No se utilizan los args.
     */
    public static void main(String[] args)
    {
        ApplicationMain app = new ApplicationMain();
        app.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
        app.setVisible(true);
    }
}
