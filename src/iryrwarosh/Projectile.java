package iryrwarosh;

import java.awt.Color;

/**
 * Clase que representa un projectile.
 */
public class Projectile
{

    //---------------------------------
    // Atributos
    //---------------------------------

    /**
     * La posicion del projectile representada en un {Punto}.
     */
    public Point position;

    /**
     * La velocidad del projectile representada en un {Punto}.
     */
    public Point velocity;

    /**
     * Instancia de la creature (Creature) de donde parte el projectile.
     */
    private Creature origin;

    /**
     * Determina si el projectile ya ha impactado con algo,
     * si el atributo es True el projectile desaparece,
     * en caso contrario (False) el projectile continua su rumbo.
     */
    private boolean isDone;

    /**
     * Nombre del projectile.
     */
    private String name;

    /**
     * El simbolo del projectile.
     */
    private char glyph;

    /**
     * El color del projectile.
     */
    private Color color;

    /**
     * El daño que ocasiona el projectile al impactar con algo.
     */
    private int damage;

    /**
     * El número de turnos que el projectile no ha impactado y
     * sigue su rumbo.
     */
    private int airTime = 0;

    //---------------------------------
    // Métodos Getter
    //---------------------------------

    /**
     * @return True si el projectile ya ha impactado con algo,
     * False en caso contrario.
     */
    public boolean isDone()
    {
        return isDone;
    }

    /**
     * @return Nombre del projectile.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return El simbolo del projectile.
     */
    public char getGlyph()
    {
        return glyph;
    }

    /**
     * @return El color del projectile.
     */
    public Color getColor()
    {
        return color;
    }

    //---------------------------------
    // Constructor
    //---------------------------------

    public Projectile(String name, Creature origin, int glyph, Color color, int damage, Point position, Point velocity)
    {
        this.name = name;
        this.position = position;
        this.velocity = velocity;
        this.glyph = (char) glyph;
        this.color = color;
        this.origin = origin;
        this.damage = damage;
    }

    /**
     * Mueve el projectile y determina si este ya ha impactado con algo.
     * @param world Instancia del mundo {World} donde el projectile se mueve.
     */
    public void moveAndCheckForCollision(World world)
    {
        // Se mueve el projectile, sumando las coordendas en (x, y)
        // de la velocidad a la posicion actual.
        position.x += velocity.x;
        position.y += velocity.y;

        // Si el projectile lleva 10 Turnos sin impactar con algo,
        // entnces el projectile desaparece.
        if (airTime++ > 10)
        {
            // El projectile desaparece.
            isDone = true;
        }

        // Si el projectile se encuentra a punto de abandonar
        // el mapa (World), destruimos el projectile y terminamos la
        // ejecución del método.
        if (!canEnter(world.tile(position.x, position.y)))
        {
            // El projectile desaparece.
            isDone = true;
            // Terminamos de ejecutar el método.
            return;
        }

        // Revisamos si el projectile ha impactado con algo.
        checkForCollision(world);
    }

    /**
     * Determina si el projectile ha impactado con algo.
     * @param world Instancia del mundo {World} donde el projectile se mueve.
     */
    public void checkForCollision(World world)
    {
        // Si en la posición donde se encuentra el projectile hay una {Creature},
        // entonces obtenemos una instancia de dicha Creatura.
        Creature creature = world.creature(position.x, position.y);

        // Si la {Creatura} es null o si la {Creatura} es amigable o si la {Creatura}
        // tiene menos de 1 corazón de vida, entonces terminamos la ejecución del método.
        // Es decir, el projectile continua su rumbo.
        if (creature == null || origin.isFriendlyTo(creature) || creature.hearts() < 1)
            return;

        // TODO: Determinar la función del siguiente trozo de método.
        if (creature.hasTrait(Trait.DEFLECT_RANGED))
        {
            MessageBus.publish(new DeflectRanged(world, creature, this));
        }
        else
        {
            creature.loseHearts(world, origin, damage, "from a distance", "You were slain by the " + getName() + " from a " + origin.name());
        }

        // El projectile desaparece.
        isDone = true;
    }

    /**
     * Determina si el projectile puede continuar su rumbo.
     * Si el projectile se encuentra en un Tile de (FUERA DE LIMITE) o (MURO BLANCO)
     * el método devolvera False (El projectile no puede continuar), pero si el
     * projectile se encuentra con cualquier otro Tile, entonces el projectile
     * podra continuar su rumbo.
     * @param tile El Tile en el cual el projectile se encuentra.
     * @return True si el projectile puede continuar su rumbo, False en caso contrario.
     */
    public boolean canEnter(Tile tile)
    {
        return tile.isFlyable();
    }
}
